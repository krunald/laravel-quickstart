# FROM php:7.4-fpm

# # Arguments defined in docker-compose.yml
# ARG user=sammy
# ARG uid=1000

# # Install system dependencies
# RUN apt-get update && apt-get install -y \
#     git \
#     curl \
#     libpng-dev \
#     libonig-dev \
#     libxml2-dev \
#     zip \
#     unzip

# # Clear cache
# RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# # Install PHP extensions
# RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd

# # Get latest Composer
# COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# # Create system user to run Composer and Artisan Commands
# RUN useradd -G www-data,root -u $uid -d /home/$user $user
# RUN mkdir -p /home/$user/.composer && \
#     chown -R $user:$user /home/$user

# # Set working directory
# WORKDIR /var/www

# USER $user

# FROM kamerk22/laravel-alpine:latest

# FROM php:7.4-fpm

# # Arguments defined in docker-compose.yml
# ARG user=sammy
# ARG uid=1000

# # Clear cache
# RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# # Create system user to run Composer and Artisan Commands
# RUN useradd -G www-data,root -u $uid -d /home/$user $user
# RUN mkdir -p /home/$user/.composer && \
#     chown -R $user:$user /home/$user

# # Set working directory
# WORKDIR /var/www

# USER $user

# ---------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------

# FROM php:7.4-fpm

# # Arguments defined in docker-compose.yml
# ARG user=travellist_user
# ARG uid=1000

# # Install system dependencies
# RUN apt-get update && apt-get install -y \
#     git \
#     curl \
#     libpng-dev \
#     libonig-dev \
#     libxml2-dev \
#     zip \
#     unzip

# # Clear cache
# RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# # Install PHP extensions
# RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd

# # Get latest Composer
# COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# COPY composer.json composer.json
# COPY composer.lock composer.lock
# RUN composer install --prefer-dist --no-scripts --no-autoloader && rm -rf /root/.composer
# ADD . .

# RUN cp .env.example .env

# # Create system user to run Composer and Artisan Commands
# RUN useradd -G www-data,root -u $uid -d /home/$user $user
# RUN mkdir -p /home/$user/.composer && \
#     chown -R $user:$user /home/$user

# # Set working directory
# WORKDIR /var/www

# USER $user



# ---------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------

FROM kamerk22/laravel-alpine:7.4-mysql-nginx

RUN apk --no-cache add pcre-dev ${PHPIZE_DEPS} libxml2-dev libressl-dev pkgconfig libevent-dev libzip-dev && apk del pcre-dev ${PHPIZE_DEPS}
RUN docker-php-ext-install zip exif soap

COPY composer.json composer.json
#COPY composer.lock composer.lock
RUN composer global require hirak/prestissimo

RUN composer install --prefer-dist --no-scripts --no-autoloader && rm -rf /root/.composer

RUN apk update && \
    apk add libxml2-dev

ADD conf/nginx/default.conf /etc/nginx/conf.d/

ADD . .

RUN cp .env.example .env
RUN cp conf/supervisor/services.ini /etc/supervisor.d/

RUN chown -R www-data:www-data \
        /var/www/storage \
        /var/www/bootstrap/cache

RUN composer dump-autoload --no-scripts --optimize
RUN ln -s /var/www/storage/app/ /var/www/public/storage
RUN chmod 777 -R storage bootstrap/cache
ENTRYPOINT [ "/usr/bin/supervisord" ]








